epicsEnvSet(PREFIX, "BBA150")
epicsEnvSet(BBA_ASYN_PORT, "BBA150")
epicsEnvSet(BBA_IP, "10.0.7.77")

require bba150

requireSnippet(bba150.cmd)

#asynSetTraceMask(${BBA_ASYN_PORT},0, 0x9)
#asynSetTraceIOMask(${BBA_ASYN_PORT},0, 0x2)
